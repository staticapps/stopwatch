# Stopwatch

Javascript Stopwatch forked from [https://github.com/TerrylBrown/javascript-stopwatch](https://github.com/TerrylBrown/javascript-stopwatch).

Added features:
- Start stop button toggle
- Made background White for dark mode

# Use Online

- [https://staticapps.gitlab.io/stopwatch](https://staticapps.gitlab.io/stopwatch)
- [https://open.lbry.com/@appsinsidelbry:5/stopwatch:0](https://open.lbry.com/@appsinsidelbry:5/stopwatch:0?r=EgyEeFKdqZtAJu9wRa88neB16YZJbiMT)


# Deployment on LBRY

Install [lpack-bash](https://gitlab.com/funapplic66/lpack-bash) then run:

```
lpack public stopwatch-xyz.lbry
```

Then upload the resulting `.lbry` file to [LBRY](https://lbry.tv).
